import matplotlib.pyplot as plt
import numpy as np

N,t = np.loadtxt('scaling_test.dat',dtype=float,delimiter="\t",unpack=True,skiprows=1)

plt.scatter(N,t)

plt.xlabel('No. CPUs')
plt.ylabel('Time (s)')
plt.title('Performing Coulomb Integral for a datagrid of ~30k points')
plt.legend()
#plt.savefig('.png')

plt.show()

