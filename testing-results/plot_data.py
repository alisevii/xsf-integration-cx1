import matplotlib.pyplot as plt
import numpy as np

xIJ,yIJ,zIJ,rIJ,phiIJ,rhoI,rhoJ,coul_int = np.loadtxt('output_testing',dtype=float,delimiter="\t",unpack=True,skiprows=1)

plt.scatter(rIJ,phiIJ,label='phiIJ')

r=np.arange(0.2,10,0.1,dtype=float)
plt.plot(r,np.divide(1,r),label='1/r',c='0')


plt.xlabel('rIJ')
plt.ylabel('phiIJ')
#plt.title('dx = 0.01')
plt.legend()
#plt.savefig('.png')

plt.show()

