#include "../incl/coul_int.h"

//Evaluates Coulomb Interaction energy for four wavefunctions ( psi1(r) psi2(r) | 1/|r-r'| | psi3(r') psi4(r') )
//nc is the number of grid points for which phi_IJ is precomputed in Mathematica, for larger distances it's approximated to 1/rIJ
void coul_int(Grid grid, std::vector<double>* psi1, std::vector<double>* psi2, std::vector<double>* psi3, std::vector<double>* psi4, std::vector<double>* phiIJnc, int nc){

//------//Declare shared variables
//	loop counters
	int i,j,k,l,m,n;
	//variable where integral value is summed to (will be "reduced" for paralelisation)
	double coul_int=0;

	#pragma omp parallel
	{
//------//Declare private variables for each thread
	//temporary variables for x,y,z coordinates in cubes I and J, as well as distance between them rIJ
	double xI,yI,zI,xJ,yJ,zJ,xIJ,yIJ,zIJ,rIJ;
	//temporary variables for charge densities and phi_IJ needed to evaluate integral
	double rhoI,rhoJ,phiIJ;
	//count 'matrix' index to avid double coutning ( IJ = JI )
	int nI,nJ;
	//indices for phiIJ (that was read in from file), representing distance between cubes
	int a,b,c;

	//loop over cube I (over r) - ijk for xyz
	#pragma omp for reduction(+:coul_int)
	for(k=0; k<grid.nz; k++){
		for(j=0; j<grid.ny; j++){
			for(i=0; i<grid.nx; i++){
				//loop over cube J (over r') - lmn for x'y'z'
				for(n=0; n<grid.nz; n++){
					for(m=0; m<grid.ny; m++){
						for(l=0; l<grid.nx; l++){
							//avoid double counting, nI = i+j*nx+k*nx*ny, nJ = l+m*nx+n*nx*ny
							nI = i + j * grid.nx + k * grid.nx * grid.ny;
							nJ = l + m * grid.nx + n * grid.nx * grid.ny;
							//only add this point if it hasn't already been add (IJ=JI)
							if(nJ>=nI){
								//cube I dimensions and charge density
								xI = grid.xmin + i*grid.dx;
								yI = grid.ymin + j*grid.dy;
								zI = grid.zmin + k*grid.dz;
								rhoI = psi1->at(nI) * psi2->at(nI);
								//cube J dimensions and charge density
								xJ = grid.xmin + l*grid.dx;
								yJ = grid.ymin + m*grid.dy;
								zJ = grid.zmin + n*grid.dz;
								rhoJ = psi3->at(nJ) * psi4->at(nJ);
								//distances between cubes I and J
								xIJ = xJ - xI;
								yIJ = yJ - yI;
								zIJ = zJ - zI;
								rIJ = sqrt( xIJ*xIJ + yIJ*yIJ + zIJ*zIJ );
								//distance between cubes in units of grid spacing
								//a,b,c corresponding to x,y,z
								a=abs(i-l);
								b=abs(j-m);
								c=abs(k-n);
								//use phiIJnc from file if distance smaller than nc in all directions
								if( a<=nc && b<=nc && c<=nc ){
									phiIJ = phiIJnc->at(c+b*(nc+1)+a*(nc+1)*(nc+1)) / grid.dx;
								}
								else{ phiIJ = 1./rIJ; }	
								//add current interaction energy
								// I=J term only once in sum
								if(nJ==nI){ coul_int += rhoI*rhoJ*phiIJ; }
								// IJ = JI, hence add twice to sum
								else if(nJ>nI){	coul_int += 2*rhoI*rhoJ*phiIJ; }
							} // if statement to avoid double counting
						} // l over x'
					} // m over y'
				} // n over z'
			} // i over x
		} // j over y
	} // k over z

	} //end pragma omp parallel
	
	//multiply sum by dVdV' to get final integral value
//	double dV2 = grid.dx*grid.dx*grid.dy*grid.dy*grid.dz*grid.dz;
//	std::cout << "Integral (sum multiplied by dxdydzdx'dy'dz') is " << coul_int*dV2 << std::endl;

	// XSF DATAGRID FROM WANNIER90 NORMALISATION
	int N = grid.nx*grid.ny*grid.nz;
	double AA2a0 = 1.889725989;  // angstrom to bohr
	double Ha2eV = 27.211396132; // Hartree to eV
	std::cout << "Coulomb integral value (in Ha) is " << coul_int*grid.Ncells*grid.Ncells/N/N/AA2a0 << std::endl;
	std::cout << "Coulomb integral value (in eV) is " << coul_int*grid.Ncells*grid.Ncells*Ha2eV/N/N/AA2a0 << std::endl;

}
