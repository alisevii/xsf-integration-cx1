#include "../incl/other_integrals.h"

//Evaluates overlap integral of two wavefunctions psi1 and psi2
void overlap_int(Grid grid, std::vector<double>* psi1, std::vector<double>* psi2){

	//Declare shared variables
	int i,j,k;
	double integral=0.0;

	#pragma omp parallel
	{	
	//Declare private variables
	int n;
	//loop over all grid
	#pragma omp for reduction(+:integral)
	for(k=0; k<grid.nz; k++){
		for(j=0; j<grid.ny; j++){
			for(i=0; i<grid.nx; i++){
				n = i + j * grid.nx + k * grid.nx * grid.ny;
				integral += psi1->at(n) * psi2->at(n);
			} //i over nx
		} //j over ny
	} //z over nz

	} //end parallel section

	//multiply sum by dV
//	double dV = grid.dx * grid.dy * grid.dz;
//	std::cout << "Overlap integral of psi1 and psi2 is " << integral*dV << std::endl;
	int N = grid.nx*grid.ny*grid.nz;
	std::cout << "Overlap integral of psi1 and psi2 is " << integral*grid.Ncells/N << std::endl;

}

//Evaluates transition dipole moment <psi1|dim|psi2>, where dim=x,y,z
void tdm_int(Grid grid, std::vector<double>* psi1, std::vector<double>* psi2, char dim){

	//Declare shared variables
	int i,j,k;
	double integral=0;
	
	#pragma omp parallel
	{
	//Declare private variables
	int n;
	double pos;

	//loop over all grid
	#pragma omp for reduction(+:integral)
	for(k=0; k<grid.nz; k++){
		for(j=0; j<grid.ny; j++){
			for(i=0; i<grid.nx; i++){
				n = i + j * grid.nx + k * grid.nx * grid.ny;
				if(dim=='x'){ pos = grid.xmin + i*grid.dx; }
				if(dim=='y'){ pos = grid.ymin + j*grid.dy; }
				if(dim=='z'){ pos = grid.zmin + k*grid.dz; }
				integral += psi1->at(n) * pos * psi2->at(n);
			} //i over nx
		} //j over ny
	} //z over nz

	} //end parallel section	

	//multiply sum by dV
//	double dV = grid.dx * grid.dy * grid.dz;
//	std::cout << "Transition dipole moment <psi1|" << dim << "|psi2> = " << integral*dV << std::endl;
	int N = grid.nx*grid.ny*grid.nz;
	double AA2a0 = 1.889725989; //convert units angstrom to bohr
	std::cout << "Transition dipole moment (in ea0) <psi1|" << dim << "|psi2> = " << integral*grid.Ncells/N*AA2a0 << std::endl;



}


