#include "../incl/grid_props.h"

//Return shifted datagrid
std::vector<double> shift_datagrid_x(Grid grid, std::vector<double>* psi, int Ncells_shift){

	std::vector<double> shifted_psi;

	int ns; //shifted index of unit cell
	int shift_pos;

	for(int k=0; k<grid.nz; k++){
		for( int j=0; j<grid.ny; j++){
			for( int n=0; n<grid.Ncells; n++){
				for( int i=0; i<grid.nx/grid.Ncells; i++){
					ns = n + Ncells_shift;
					//zero psi if it goes outside of supercell
					if(ns>=grid.Ncells || ns<0){
						shifted_psi.push_back(0);
						std::cout << "adding zero" << std::endl;
					}
					//get index where to get the correct datagird number from
					else{
						shift_pos = i + ns*grid.nx/grid.Ncells + j*grid.nx + k*grid.ny*grid.nx;
						shifted_psi.push_back( psi->at(shift_pos) );
					}
				} //for i in nx/Ncells
			} //for n in Ncells
		} //for j in ny
	} //for k in nz

	return shifted_psi;
}

//Return shifted datagrid
std::vector<double> shift_datagrid_z(Grid grid, std::vector<double>* psi, int Ncells_shift){

	std::vector<double> shifted_psi;

	int ns; //shifted index of unit cell
	int shift_pos;

	for( int n=0; n<grid.Ncells; n++){
		for(int k=0; k<grid.nz/grid.Ncells; k++){
			for( int j=0; j<grid.ny; j++){
				for( int i=0; i<grid.nx; i++){
					ns = n + Ncells_shift;
					//zero psi if it goes outside of supercell
					if(ns>=grid.Ncells || ns<0){
						shifted_psi.push_back(0);
					}
					//get index where to get the correct datagird number from
					else{
						shift_pos = i + j*grid.nx + k*grid.ny*grid.nx + ns*grid.nz/grid.Ncells*grid.ny*grid.nx; 
						shifted_psi.push_back( psi->at(shift_pos) );
					}
				} //for i in nx
			} //for j in ny
		} //for k in nz
	} //for n in Ncells

	return shifted_psi;
}

//Store charge density datagrid from file to vector
void datagrid2vector(std::vector<double>* rho, std::string datagrid_filename, int Npts){
	//file where datagrid lives
	std::ifstream infile(datagrid_filename);	
	//skip first 5 lines where grid parameters are
	std::string skip;
	for(int i=0; i<5; i++){ std::getline(infile,skip); }
	//There's 6 points per line (+1 because remainder adds up to a whole line)
	int Nlines = (int)Npts/6 + 1;
	int remain = Npts%6;
	//declare some variables needed for reading in
	std::string line;			//dummy label for any line
	std::vector<std::string> input;		//vector of strings (one line)
	std::string token; 			//dummy label for elements of input string
	//assuming that datagrid is written in column major order (i over x runs fastest)
	//this is default for datagrids in xsf files
	for(int n=0; n<Nlines; n++){
		std::getline(infile, line);			//read in line
		std::stringstream ss(line);			//convert to stringstream
		while(ss >> token){input.push_back(token);}	//put each element of line into input
		if(n==Nlines-1){	//last line, only remainder left
			for(int i=0; i<remain; i++){ rho->push_back(atof(input.at(i + n*6).c_str())); }
		}
		else{ 			//all other lines, read in all 5 numbers
			for(int i=0; i<6; i++){ rho->push_back(atof(input.at(i + n*6).c_str())); }
		}
	} //end for over Nlines
/*
	------------------- TO DO -----------------------
	Might be worth introducing some error message
	if datagrid that's read in has wrong no. gridpoints
*/
}

//Read in grid parameters from file (just top of xsf)
Grid read_grid_params(std::string grid_params_file){

	Grid grid;				//declare grid (or might just pass reference)
	std::ifstream infile(grid_params_file); //open file for reading
	std::string line;			//dummy label for any line
	std::vector<std::string> input;		//vector of strings (one line)
	std::string token; 			//dummy label for elements of input string
	//first line - nx, ny, nz
	std::getline(infile, line); 		   //read in the line
	std::stringstream ns(line); 		   //magical string stream
	while(ns >> token){input.push_back(token);}//put elements of line into the vector of strings
	grid.nx=::atof(input.at(0).c_str());       //assign read in values to grid attributes 
	grid.ny=::atof(input.at(1).c_str());
	grid.nz=::atof(input.at(2).c_str());
	//second line is origin - xmin, ymin, zmin
	std::getline(infile, line); 	
	std::stringstream origin(line); 
	while(origin >> token){input.push_back(token);}
	grid.xmin=::atof(input.at(3).c_str()); 
	grid.ymin=::atof(input.at(4).c_str());
	grid.zmin=::atof(input.at(5).c_str());
	//third line - x spanning vector
	std::getline(infile, line); 	
	std::stringstream xspan(line); 
	while(xspan >> token){input.push_back(token);}
	grid.dx = atof(input.at(6).c_str())/(grid.nx-1); //calculate grid spacing from spanning vector
	//fourth line - y spanning vector
	std::getline(infile, line); 	
	std::stringstream yspan(line); 
	while(yspan >> token){input.push_back(token);}
	grid.dy = atof(input.at(10).c_str())/(grid.ny-1); 
	//fifth line - z spanning vector
	std::getline(infile, line); 	
	std::stringstream zspan(line); 
	while(zspan >> token){input.push_back(token);}
	grid.dz = atof(input.at(14).c_str())/(grid.nz-1); 
	//All done, return grid to main to continue 
	return grid;
}


//Read in precomputed (Mathematica) values for phiIJ for nc gridpoints around singularity
void phiIJnc2vector(std::vector<double>* phiIJnc, std::string phiIJ_file,int nc){

	int Nc=(nc+1)*(nc+1)*(nc+1);		//this many lines in the file
	std::ifstream infile(phiIJ_file); 	//open file for reading
	std::string line;			//dummy label for any line
	std::vector<std::string> input;		//vector of strings (one line)
	std::string token; 			//dummy label for elements of input string
	//loop over all lines and read in phiIJ
	for(int i=0; i<Nc; i++){
		std::getline(infile, line); 		   //read in the line
		std::stringstream ss(line); 		   //magical string stream
		while(ss >> token){input.push_back(token);}//put elements of line into the vector of strings
		//phi is in fourth column (thus 3), need to add 4 in each loop of i
		phiIJnc->push_back(atof(input.at(3+i*4).c_str()));
	}//i over Nc
} //end phiIJnc2vector

