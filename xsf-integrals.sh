#!/bin/sh
#PBS -l walltime=24:00:00
#PBS -l select=1:ncpus=24:mem=62000MB
#PBS -m e

module load intel-suite

cp -r $PBS_O_WORKDIR/* ./ 

chmod 755 run_main.sh
export OMP_NUM_THREADS=24
./run_main.sh > output

cp -r * $PBS_O_WORKDIR

