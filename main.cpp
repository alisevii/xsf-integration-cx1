#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <cmath>
#include <string>

#include "incl/grid_props.h"
#include "incl/coul_int.h"
#include "incl/other_integrals.h"

//--------------- MAIN ------------------//
int main(int argc, char* argv[]){

	// NCELLS NEEDED FOR XSF DATAGRID NORMALISATION
	int Ncells=16;
	//add this to run_main.sh later

	//Declare grids and assign filenames
	Grid psi1_gr,psi2_gr;
	std::string file1=argv[1];	
	std::string file2=argv[2];

	//read in grid parameters
	psi1_gr = read_grid_params(file1);
	psi2_gr = read_grid_params(file2);

	//datapoints in grid of all psis
	int N1 = psi1_gr.nx * psi1_gr.ny * psi1_gr.nz;
	int N2 = psi2_gr.nx * psi2_gr.ny * psi2_gr.nz;
	//Check if no. grid points are equal
	if( N1 == N2 ){ std::cout << "Jackpot, grids are the same size!" << std::endl; }
	else{ std::cout << "ERROR! Grids are different sizes! Exiting..." << std::endl; return 0; }

	//and save datagrid to vectors psi1 etc.
	std::vector<double> psi1,psi2;
	datagrid2vector(&psi1,file1,N1);
	datagrid2vector(&psi2,file2,N2);
	
	//add Ncells to all grids
	psi1_gr.Ncells=Ncells;
	psi2_gr.Ncells=Ncells;

	//SHIFT GRID HERE IF NEEDED
	//shift psi2 back by one unit cell (Ncells_shift = +1) 
//	std::vector<double> psi2_sh_1 = shift_datagrid(psi2_gr,&psi2,1);
	//shift psi1 forward by one unit cell (Ncells_shift = -1)
//	std::vector<double> psi1_sh_1 = shift_datagrid_z(psi1_gr,&psi1,1);
//	std::vector<double> psi1_sh_2 = shift_datagrid_z(psi1_gr,&psi1,2);

	//Some output to screen for testing purposes mainly
	std::cout << "Grid parameters read in from " << file1 << std::endl;
	std::cout << "Grid origin at (" << psi1_gr.xmin << "," << psi1_gr.ymin << "," << psi1_gr.zmin << ")" << std::endl;
	std::cout << "No. grid points = " << psi1_gr.nx << " * " << psi1_gr.ny << " * " << psi1_gr.nz << " = " << N1 << std::endl; 
	std::cout << "Grid spacing dx = " << psi1_gr.dx << ", dy = " << psi1_gr.dy << ", dz = " << psi1_gr.dz << std::endl;
	std::cout << "Ncells used in DFT and wannierisation is " << psi1_gr.Ncells << std::endl;

	// Read in precomputed values for phi for nc points around the 1/|rI-rJ| singularity
	int nc=atoi(argv[5]);			//critical radius (units of gridspacing)
	std::string phiIJ_file=argv[6];		//file with precomputed values
	std::vector<double> phiIJnc;		//store precomputed values in phiIJnc
	phiIJnc2vector(&phiIJnc,phiIJ_file,nc);
	std::cout << nc << " grid points around singularity will be calculated using precomputed values from " << phiIJ_file << std::endl;	

	std::cout << "SHIFTED VAL BY +1 UNIT CELL" << std::endl;
	//OVERLAP INTEGRALS
	std::cout << "Overlap of <val|val>:" << std::endl;
	overlap_int(psi1_gr,&psi1,&psi1);
	std::cout << "Overlap of <cond|cond>:" << std::endl;
	overlap_int(psi1_gr,&psi2,&psi2);
	std::cout << "Overlap of <val|cond>:" << std::endl;
	overlap_int(psi1_gr,&psi1,&psi2);
	//TRANSITION DIPOLE MOMENTS
	std::cout << "<val|x|cond>:" <<std::endl;
	tdm_int(psi1_gr,&psi1, &psi2, 'x');
	std::cout << "<val|y|cond>:" <<std::endl;
	tdm_int(psi1_gr,&psi1, &psi2, 'y');
	std::cout << "<val|z|cond>:" <<std::endl;
	tdm_int(psi1_gr,&psi1, &psi2, 'z');
	//EVALUATE COULOMB INTEGRALS
	std::cout << "Direct coulomb integral <val cond | 1/r | val cond >:" << std::endl;
	coul_int(psi1_gr,&psi1,&psi1,&psi2,&psi2,&phiIJnc,nc);
//	std::cout << "Exchange coulomb integral <val cond | 1/r | cond val >:" << std::endl;
//	coul_int(psi1_gr,&psi1,&psi2,&psi2,&psi1,&phiIJnc,nc);

	return 0;
}
