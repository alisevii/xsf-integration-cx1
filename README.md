# Evaluating Integrals for XSF datagrids

## Instructions

1. To compile on cx1, type `module load intel-suite` and `make`.
2. Provide input file paths in `run_main.sh` and run the script.
3. A script to submit the calculation to the PBS queueing system is provided `xsf-integrals.sh`

I'm using the icpc compiler from the intel-suite, which can be changed in the first line of the Makefile. Only dependency is OpenMP (-qopenmp compiler flag at top of Makefile).

## Description

The code reads in the grid parameters and datagrid values from an xsf file and performs overlap integrals and computes transition dipole moments and the direct and exchange coulomb term. 
At the moment the code reads in two wavefunctions, inteded for one valence state and one conduction state.
It can be easily extended to take in four different wavefunctions if needed, however most models limit the Hamiltonian matrix elements to only include those terms where the electron (and hole) are on the same unit in both basis states.
The issue with the singularity due to 1/|r-r'| has been solved as described in [this](http://aip.scitation.org/doi/abs/10.1063/1.1610438) paper. The phi_IJ = 1/|rI-rJ| values where rI and rJ are close are precomputed (using NIntegrate in Mathematica) and stored in a file that the code reads in. Values of phi_IJ for rI and rJ further apart are approximate to 1/(rI-rJ). 

## Contents

* `main.cpp` does all the reading in of grid parameters and datagrids from files, shifting them and passing correct wavefunctions to integrating functions.
* `src/grid_props.cpp` has all functions to do with reading in and shifting datagrids etc.
* `src/coul_int.cpp` is the heart of the code - given four wavefucntions, performs the 6-dimensional integral
* `src/other_ints.cpp` does the simple integrals, which were fine in python, but I've included in here for completeness sake
* header files for all three are in the incl folder
