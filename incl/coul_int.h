#ifndef COUL_INT_H
#define COUL_INT_H

#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>
#include <algorithm> //sort
#include "grid_props.h"

void coul_int(Grid grid, std::vector<double>* psi1, std::vector<double>* psi2, std::vector<double>* psi3, std::vector<double>* psi4, std::vector<double>* phiIJnc, int nc);

#endif
