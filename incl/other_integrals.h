#ifndef OTHER_INTEGRALS_H
#define OTHER_INTEGRALS_H

#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>
#include <algorithm> //sort
#include "grid_props.h"

void overlap_int(Grid grid, std::vector<double>* psi1, std::vector<double>* psi2);

void tdm_int(Grid grid, std::vector<double>* psi1, std::vector<double>* psi2, char dim);

#endif
