#ifndef GRID_PROPS_H
#define GRID_PROPS_H

#include <cmath>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>

//Store all grid parameters in one struct
struct Grid {
	double xmin,ymin,zmin;	//grid origin
	double xmax,ymax,zmax;	//grid 'end'
	double dx,dy,dz;	//grid spacing
	int nx,ny,nz;		//no. grid points
	int Ncells;		//Ncells needed for xsf grid normalisation
};

//Return shifted datagrid
std::vector<double> shift_datagrid_x(Grid grid, std::vector<double>* psi, int Ncells_shift);
std::vector<double> shift_datagrid_z(Grid grid, std::vector<double>* psi, int Ncells_shift);

//Read in grid parameters from file (just top of xsf)
Grid read_grid_params(std::string grid_params_file);

//Store charge density datagrid from file to vector
void datagrid2vector(std::vector<double>* rho, std::string daragrid_filename, int Npts);

//Read in precomputed values of phiIJ, nc gridpoints around singularity
void phiIJnc2vector(std::vector<double>* phiIJnc, std::string phiIJ_file, int nc);

#endif
