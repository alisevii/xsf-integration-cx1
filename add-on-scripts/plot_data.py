import matplotlib.pyplot as plt
import numpy as np

#x,r,rho = np.loadtxt('rho_r_dx_1e-2.dat',dtype=float,delimiter='\t',unpack=True,skiprows=1)
x,y,z,r,rho = np.loadtxt('test_density.dat',dtype=float,delimiter="\t",unpack=True,skiprows=1)

plt.scatter(x,rho,label='rho')

#plt.xlabel('$r=\sqrt{x^2+y^2+z^2}$')
plt.xlabel('x')
plt.ylabel('Charge Density')
#plt.title('dx = 0.01')
plt.legend()
#plt.savefig('.png')

plt.show()

