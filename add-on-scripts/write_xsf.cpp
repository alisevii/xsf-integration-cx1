#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <cmath>
#include <string>

//----------- TEST CHARGE DENSITY ----------//

//functio declaration
double rho_eval(double x, double y, double z);
//Evaluates charge density rho at a certain x,y,z grid-cube
double rho_eval(double x, double y, double z){
	//Lara's example psi = L00 * exp(-r^2), where L00 = 0.5/sqrt(pi)
	double r2 = x*x+y*y+z*z;
//	double r2 = (x-1)*(x-1)+(y-1)*(y-1)+(z-1)*(z-1);
//	return exp(-2*r2)/(4*M_PI);
	return 1.0;
} 

//----------------- MAIN ------------------//
int main(int argc, char* argv[]){

	//-----PARAMETERS-----//
	double xmin=-3, xmax=3;		//grid range (non-zero charge density outside)
	double ymin=-3, ymax=3;
	double zmin=-3, zmax=3;
	double dx=1.0;			//grid spacing in all directions
	double dy=1.0;
	double dz=1.0;
	int nx=(xmax-xmin)/dx+1;	//gridpoints in all directions
	int ny=(ymax-ymin)/dy+1;
	int nz=(zmax-zmin)/dz+1;

	//filename for datagrid (need to convet to const char* to make c++ happy)
	std::string filename="test_density.dat";
	const char* rho_filename=filename.c_str();

	//temporary varibales for looping
	double xi,yj,zk,rho;
	// Write density grid to file
	std::ofstream output_rho;
	output_rho.open(rho_filename);
	//counter to check every fifth line for new line in file
	int count=1;
	// Loop through grid-cubes ijk for xyz
	for(int k=0; k<nz; k++){
		for(int j=0; j<ny; j++){
			for(int i=0; i<nx; i++){
				//temporary variables for x, y and z
				xi = xmin + i*dx;
				yj = ymin + j*dy;
				zk = zmin + k*dz;
				//get charge density at current grid point
				rho=rho_eval(xi,yj,zk);
				//write to file
				output_rho << "\t" << rho;
				//new line only if every fifth
				if(count % 6 == 0){ output_rho << std::endl; }
				//update counter
				count++;
			}
		}
	}
	output_rho.close();

	return 0;
}
