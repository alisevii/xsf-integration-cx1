CXX = icpc
CXXFLAGS = -std=c++11 -qopenmp

EXE = main
SRC = main.cpp src/coul_int.cpp src/grid_props.cpp src/other_integrals.cpp
OBJ = $(SRC: .cpp=.o)

all: $(SRC) $(EXE)

$(EXE): $(OBJ)
	$(CXX) $(CXXFLAGS) $(OBJ) -o $@

clean:
		rm $(EXE) $(OBJ)
