#!/bin/bash

psi1="input/PA_kz16_val.xsf"
psi2="input/PA_kz16_cond.xsf"
psi3="placeholder"
psi4="placeholder"
nc="16"
phiIJ_file="input/phiIJ_nc16.dat"

MAIN="main"

COMMAND="MAIN psi1 psi2 psi3 psi4 nc phiIJ_file"
RUNCOMMAND="$MAIN $psi1 $psi2 $psi3 $psi4 $nc $phiIJ_file"

#echo $COMMAND
#echo $RUNCOMMAND
./$RUNCOMMAND 

